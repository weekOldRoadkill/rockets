// Namespaces
pub(crate) use crate::{pid::*, rocket::*};
pub(crate) use bevy::{prelude::*, *};
pub(crate) use bevy_panorbit_camera::*;
pub(crate) use bevy_rapier3d::prelude::*;
pub(crate) use iyes_perf_ui::prelude::*;
pub(crate) use rand::prelude::*;
pub(crate) use serde::*;
pub(crate) use std::{ops::*, result::*, *};

// Constants
pub(crate) const AMBIENT_COLOR: Color = Color::srgb(
    0x1D as f32 / 255.0,
    0x20 as f32 / 255.0,
    0x21 as f32 / 255.0,
);
const SUN_COLOR: Color = Color::srgb(
    0xFB as f32 / 255.0,
    0xF1 as f32 / 255.0,
    0xC7 as f32 / 255.0,
);
const CAMERA_FOV: f32 = 80.0;
const GRID_LENGTH: f32 = 100.0;
const GRID_WIDTH: f32 = 0.05;

// Error enum
#[derive(thiserror::Error, Debug)]
pub(crate) enum RocketsErr {
    #[error("Set logger error: {0}")]
    SetLoggerError(#[from] ::log::SetLoggerError),
}

// Focus resource struct
#[derive(Resource)]
pub(crate) struct Focus {
    camera: Entity,
    pub(crate) target: Entity,
}

// Focus resource implementation
impl Focus {
    // Focus camera
    pub(crate) fn focus(
        focus: Option<Res<Focus>>,
        mut cameras: Query<&mut PanOrbitCamera>,
        targets: Query<(&Transform, &Velocity)>,
    ) -> () {
        (|| {
            let focus = focus?;
            let (transform, vel) = targets.get(focus.target).ok()?;
            cameras.get_mut(focus.camera).ok()?.target_focus =
                transform.translation + 0.02 * vel.linvel;
            Some(())
        })();
    }
}

// Focus resource from world implementation
impl FromWorld for Focus {
    // Creates a new focus from the given world
    fn from_world(world: &mut World) -> Self {
        Self {
            camera: world.spawn_empty().id(),
            target: world.spawn_empty().id(),
        }
    }
}

// Spawn camera
pub(crate) fn spawn_camera(mut commands: Commands, focus: Option<ResMut<Focus>>) -> () {
    (|| {
        let mut focus = focus?;
        focus.camera = commands
            .spawn((
                Camera3dBundle {
                    camera: Camera {
                        clear_color: ClearColorConfig::Custom(AMBIENT_COLOR),
                        ..Default::default()
                    },
                    projection: Projection::Perspective(PerspectiveProjection {
                        fov: CAMERA_FOV * f32::consts::PI / 180.0,
                        ..Default::default()
                    }),
                    transform: Transform::from_xyz(10.0, 0.0, 0.0).looking_at(Vec3::ZERO, Dir3::Y),
                    ..Default::default()
                },
                PanOrbitCamera {
                    pan_sensitivity: 0.5,
                    pan_smoothness: 0.005,
                    ..Default::default()
                },
                pbr::ScreenSpaceAmbientOcclusionBundle {
                    settings: pbr::ScreenSpaceAmbientOcclusionSettings {
                        quality_level: pbr::ScreenSpaceAmbientOcclusionQualityLevel::Ultra,
                    },
                    ..Default::default()
                },
            ))
            .id();
        Some(())
    })();
}

// Spawn light
pub(crate) fn spawn_light(mut commands: Commands) -> () {
    commands.spawn(DirectionalLightBundle {
        directional_light: DirectionalLight {
            color: SUN_COLOR,
            shadows_enabled: true,
            ..Default::default()
        },
        cascade_shadow_config: pbr::CascadeShadowConfigBuilder {
            num_cascades: 8,
            maximum_distance: 10000.0,
            ..Default::default()
        }
        .build(),
        transform: Transform::default().looking_to(Vec3::new(-1.0, -1.0, 1.0), Dir3::Y),
        ..Default::default()
    });
}

// Exit app on escape
pub(crate) fn exit_on_escape(
    keys: Option<Res<ButtonInput<KeyCode>>>,
    mut app_exit: EventWriter<AppExit>,
) -> () {
    (|| {
        let keys = keys?;
        if keys.just_pressed(KeyCode::Escape) {
            app_exit.send(AppExit::Success);
        }
        Some(())
    })();
}

// Disable gravity
pub(crate) fn disable_gravity(conf: Option<ResMut<RapierConfiguration>>) -> () {
    (|| {
        let mut conf = conf?;
        conf.gravity = Vec3::ZERO;
        Some(())
    })();
}

// Spawn grid
pub(crate) fn spawn_grid(
    mut commands: Commands,
    meshes: Option<ResMut<Assets<Mesh>>>,
    materials: Option<ResMut<Assets<StandardMaterial>>>,
) -> () {
    (|| {
        let mut meshes = meshes?;
        let mut materials = materials?;
        let mesh = meshes.add(Cuboid::default());
        let material = materials.add(StandardMaterial::from(SUN_COLOR));
        for scale in [
            Vec3::new(GRID_LENGTH, GRID_WIDTH, GRID_WIDTH),
            Vec3::new(GRID_WIDTH, GRID_LENGTH, GRID_WIDTH),
            Vec3::new(GRID_WIDTH, GRID_WIDTH, GRID_LENGTH),
        ] {
            commands.spawn(PbrBundle {
                mesh: mesh.clone(),
                material: material.clone(),
                transform: Transform::from_scale(scale),
                ..Default::default()
            });
        }
        Some(())
    })();
}

// Spawn overlay
pub(crate) fn spawn_overlay(mut commands: Commands) -> () {
    commands.spawn((PerfUiRoot::default(), PerfUiEntryFPS::default()));
}
