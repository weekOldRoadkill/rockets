// Namespaces
use crate::util::*;

// Rocket component struct
#[derive(Component)]
pub(crate) struct Rocket {
    throttle: f32,
    gimbal: Vec2,
    thruster_angle: f32,
    thruster_rot: Quat,
    thruster: Entity,
    stability_pid: PID<Vec2>,
    stability_score: f32,
    target: Entity,
}

// Rocket component implementation
impl Rocket {
    // Constants
    const GIMBAL_LIMIT: f32 = f32::consts::PI / 12.0;
    const MAX_VEL: f32 = 75.0;
    const GEN_COUNT: usize = 50;
    const MAX_THRUST: f32 = 5.0;

    // Spawn a new rocket
    fn spawn_inner(
        mut commands: Commands,
        assets: &RocketAssets,
        transform: Transform,
        target: Entity,
        stability_pid: PIDBuilder<Vec2>,
    ) -> () {
        let thruster = commands
            .spawn(SceneBundle {
                scene: assets.thruster.clone(),
                ..Default::default()
            })
            .id();
        commands
            .spawn((
                Self {
                    throttle: 0.0,
                    gimbal: Vec2::ZERO,
                    thruster_angle: 0.0,
                    thruster_rot: Quat::IDENTITY,
                    thruster,
                    stability_pid: stability_pid.into(),
                    stability_score: 0.0,
                    target,
                },
                RigidBody::Dynamic,
                Collider::compound(vec![(
                    0.55 * Vec3::Y,
                    Quat::default(),
                    Collider::round_cone(1.6, 0.5, 0.5),
                )]),
                SpatialBundle {
                    transform,
                    ..Default::default()
                },
                Velocity::default(),
                ExternalForce::default(),
                ActiveCollisionTypes::empty(),
                assets.body.clone(),
            ))
            .add_child(thruster);
    }

    // Spawn a generation of rockets
    pub(crate) fn spawn_gen(mut commands: Commands, assets: Option<Res<RocketAssets>>) -> () {
        (|| {
            let mut rng = thread_rng();
            let assets = assets?;
            let transform = Transform::from_rotation({
                let mut f = || rng.gen_range(0.0..f32::consts::TAU);
                Quat::from_euler(EulerRot::XYZ, f(), f(), f())
            });
            let target = commands
                .spawn(PbrBundle {
                    mesh: assets.target_mesh.clone(),
                    material: assets.target_material.clone(),
                    transform: Transform::from_translation({
                        let mut f = || rng.gen_range(-50.0..50.0);
                        Vec3::new(f(), f(), f())
                    }),
                    ..Default::default()
                })
                .id();
            for _ in 0..Self::GEN_COUNT {
                Self::spawn_inner(commands.reborrow(), &assets, transform, target, {
                    let mut f = || rng.gen_range(0.0..5.0);
                    let mut f = || Vec2::new(f(), f());
                    PIDBuilder {
                        kp: f(),
                        ki: 0.1 * f(),
                        kd: 0.5 * f(),
                        ..Default::default()
                    }
                });
            }
            Some(())
        })();
    }

    // Run the rockets' PIDs
    pub(crate) fn run_pids(
        mut rockets: Query<(&mut Rocket, &Velocity)>,
        time: Option<Res<Time>>,
    ) -> () {
        (|| {
            let delta = time?.delta_seconds();
            for (mut rocket, vel) in &mut rockets {
                rocket.throttle = 1.0;
                rocket.gimbal = rocket.stability_pid.update(-vel.angvel.xz(), delta);
            }
            Some(())
        })();
    }

    // Update the thruster data of the rockets
    pub(crate) fn update_thrusters(
        mut rockets: Query<&mut Rocket>,
        mut transforms: Query<&mut Transform>,
        time: Option<Res<Time>>,
    ) -> () {
        (|| {
            let time = time?.elapsed_seconds();
            for mut rocket in &mut rockets {
                rocket.throttle = rocket.throttle.clamp(0.0, 1.0);
                rocket.gimbal = rocket.gimbal.clamp_length_max(Self::GIMBAL_LIMIT);
                rocket.thruster_rot =
                    Quat::from_rotation_x(rocket.gimbal.x) * Quat::from_rotation_z(rocket.gimbal.y);
                let mut thruster = transforms.get_mut(rocket.thruster).ok()?;
                let angle = 50.0 * time;
                if angle - rocket.thruster_angle > 5.0 {
                    rocket.thruster_angle = angle;
                }
                let throttle = (0.05 * rocket.thruster_angle.sin() + 1.0) * rocket.throttle;
                thruster.translation.y = 1.25 * (throttle - 1.0);
                thruster.rotation = rocket.thruster_rot;
                thruster.rotate_local_y(rocket.thruster_angle);
                thruster.scale = Vec3::splat(throttle);
            }
            Some(())
        })();
    }

    // Apply the thrust to the rockets
    pub(crate) fn apply_thrust(
        mut rockets: Query<(&Rocket, &Transform, &mut ExternalForce)>,
    ) -> () {
        for (rocket, transform, mut force) in &mut rockets {
            *force = ExternalForce::at_point(
                transform.rotation
                    * rocket.thruster_rot
                    * Self::MAX_THRUST
                    * Vec3::Y
                    * rocket.throttle,
                transform.transform_point(-1.5 * Vec3::Y),
                transform.transform_point(Vec3::ZERO),
            );
        }
    }

    // Limit velocities
    pub(crate) fn limit_vels(mut rockets: Query<&mut Velocity, With<Rocket>>) -> () {
        for mut vel in &mut rockets {
            vel.linvel = vel.linvel.clamp_length_max(Self::MAX_VEL);
        }
    }

    // Calculate scores
    pub(crate) fn calc_scores(
        mut rockets: Query<(&mut Rocket, &Velocity)>,
        time: Option<Res<Time>>,
    ) -> () {
        (|| {
            let delta = time?.delta_seconds();
            for (mut rocket, vel) in &mut rockets {
                rocket.stability_score += vel.angvel.xz().length() * delta;
            }
            Some(())
        })();
    }

    // Focus on the best rocket
    pub(crate) fn focus_best(
        rockets: Query<(&Rocket, Entity)>,
        focus: Option<ResMut<Focus>>,
    ) -> () {
        (|| {
            let mut focus = focus?;
            focus.target = rockets
                .iter()
                .min_by(|(rocket0, _), (rocket1, _)| {
                    rocket0
                        .stability_score
                        .partial_cmp(&rocket1.stability_score)
                        .unwrap_or(cmp::Ordering::Equal)
                })?
                .1;
            Some(())
        })();
    }
}

// Rocket assets resource struct
#[derive(Resource)]
pub(crate) struct RocketAssets {
    body: Handle<Scene>,
    thruster: Handle<Scene>,
    target_mesh: Handle<Mesh>,
    target_material: Handle<StandardMaterial>,
}

// Rocket assets implementation
impl RocketAssets {
    // Constants
    const TARGET_COLOR: Color = Color::srgb(
        0xFB as f32 / 255.0,
        0x49 as f32 / 255.0,
        0x34 as f32 / 255.0,
    );

    // Load rocket assets
    pub(crate) fn load(
        mut commands: Commands,
        assets: Option<Res<AssetServer>>,
        meshes: Option<ResMut<Assets<Mesh>>>,
        materials: Option<ResMut<Assets<StandardMaterial>>>,
    ) -> () {
        (|| {
            let assets = assets?;
            let mut meshes = meshes?;
            let mut materials = materials?;
            commands.insert_resource(Self {
                body: assets.load("rocket/body.glb#Scene0"),
                thruster: assets.load("rocket/thruster.glb#Scene0"),
                target_mesh: meshes.add(Sphere::new(0.25)),
                target_material: materials.add(StandardMaterial::from(Self::TARGET_COLOR)),
            });
            Some(())
        })();
    }
}
