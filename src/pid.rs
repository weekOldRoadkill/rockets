// Namespaces
use crate::util::*;

// PID controller builder struct
#[derive(Default, Serialize, Deserialize)]
pub(crate) struct PIDBuilder<T> {
    pub(crate) kp: T,
    pub(crate) ki: T,
    pub(crate) kd: T,
    pub(crate) bias: T,
}

// PID controller struct
#[derive(Default)]
pub(crate) struct PID<T> {
    builder: PIDBuilder<T>,
    integral: T,
    deriv: T,
    error: T,
}

// PID controller implementation
impl<T> PID<T>
where
    T: Default
        + Copy
        + AddAssign
        + Mul<f32, Output = T>
        + Sub<Output = T>
        + Div<f32, Output = T>
        + Mul<Output = T>
        + Add<Output = T>
        + PartialEq
        + fmt::Debug,
{
    // Update the PID controller
    pub(crate) fn update(self: &mut Self, error: T, delta: f32) -> T {
        self.integral += error * delta;
        self.deriv = (error - self.error) / delta;
        self.error = error;
        let output = self.builder.kp * self.error
            + self.builder.ki * self.integral
            + self.builder.kd * self.deriv
            + self.builder.bias;
        if output != output {
            T::default()
        } else {
            output
        }
    }
}

// PID controller from PID controller builder implementation
impl<T> From<PIDBuilder<T>> for PID<T>
where
    T: Default,
{
    // Creates a new PID controller from the given PID controller builder
    fn from(builder: PIDBuilder<T>) -> Self {
        Self {
            builder,
            ..Default::default()
        }
    }
}
