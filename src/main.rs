// Modules
mod pid;
mod rocket;
mod util;

// Namespaces
use crate::util::*;

// Main function
fn main() -> Result<(), RocketsErr> {
    env_logger::try_init()?;
    App::new()
        .add_plugins((
            DefaultPlugins
                .build()
                .disable::<log::LogPlugin>()
                .add(WindowPlugin {
                    primary_window: Some({
                        let name: String = "Rockets".into();
                        let mut window = Window {
                            present_mode: window::PresentMode::AutoVsync,
                            mode: window::WindowMode::BorderlessFullscreen,
                            title: name.clone(),
                            name: Some(name),
                            resizable: true,
                            focused: true,
                            window_theme: Some(window::WindowTheme::Dark),
                            ..Default::default()
                        };
                        window.set_maximized(true);
                        window
                    }),
                    ..Default::default()
                }),
            PanOrbitCameraPlugin,
            RapierPhysicsPlugin::<NoUserData>::default(),
            PerfUiPlugin,
            diagnostic::FrameTimeDiagnosticsPlugin::default(),
        ))
        .init_resource::<Focus>()
        .insert_resource(Msaa::Sample8)
        .insert_resource(pbr::DirectionalLightShadowMap { size: 4096 })
        .insert_resource(AmbientLight {
            color: AMBIENT_COLOR,
            brightness: 25000.0,
        })
        .add_systems(
            Startup,
            (
                spawn_camera,
                spawn_light,
                (RocketAssets::load, Rocket::spawn_gen).chain(),
                disable_gravity,
                spawn_grid,
                spawn_overlay,
            ),
        )
        .add_systems(
            Update,
            (
                exit_on_escape,
                Focus::focus,
                Rocket::run_pids,
                Rocket::update_thrusters,
                Rocket::apply_thrust,
                Rocket::limit_vels,
                Rocket::calc_scores,
                Rocket::focus_best,
            ),
        )
        .run();
    Ok(())
}
